new gridjs.Grid({
    columns: ["Name", "Email"],
    data: [
      ["Edi Hartono", "edi.eduwork@gmail.com"],
      ["Dodi Prakoso", "dodi@upscale.com"],
      ["Eoin", "eoin@gmail.com"],
      ["Sarah", "sarahcdd@gmail.com"],
      ["Afshin", "afshin@mail.com"]
    ]
  }).render(document.getElementById("wrapper"))